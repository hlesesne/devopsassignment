const Product = require('../models/product.model');

//Simple version
exports.test = function (req, res) {
    res.send('Greetings from the Test controller, test presentazione!!!');
};

exports.product_create = function (req, res, err) {
    let product = new Product(
        {
            name: req.body.name,
            price: req.body.price
        }
    );

    product.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Product Created successfully')
    })
};

exports.product_details_name = function (req, res, err) {
    Product.findOne({name:req.params.name}, function (err, product) {
        if (err) return next(err);
        res.send(product);
    })
};

exports.product_update_name = function (req, res, err) {
    Product.findOneAndUpdate({name: req.params.name}, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('Product udpated.');
    });
};

exports.product_delete_name = function (req, res, err) {
    Product.findOneAndRemove({name: req.params.name}, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};