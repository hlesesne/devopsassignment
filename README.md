# DevOps Assignment
_Pietro Parini 794146_  
(progetto svolto dasolo)


Repository del progetto:<br/>    
  https://gitlab.com/pietroparini2/devopsassignment/<br/>  
Ambiente di distribuzione app:<br/>   
  https://dashboard.heroku.com/apps/simple-app-devops/<br/>   
Ambiente di distribuzione db: <br/>  
  https://mlab.com/databases/simpledb/


## Scopo del progetto
Questo progetto è sviluppato con lo scopo di distribuire un'applicazione
tramite una pipeline CI/CD.

## Tecnologie usate
Lo scopo è quello di distribuire un' applicazione **node.js** da una repository
GitLab tramite **GitLab CI/CD**.
La distribuzione dell' applicazione avverrà in maniera subordinata al passaggio 
di una suite di test unitari.
 
 
## Applicazione
L'applicazione node.js sviluppata con lo scopo di provare l'ambiente CI/CD è una
semplice **API**, che effettua operazioni CRUD su un database **MongoDB**. 
L'app sarà sviluppata secondo il modello MVC.
M: il modello contiene la definizione delo schema utilizzato 
V: il router contiene le rotte a cui vengono fatte le richieste HTTP
C: il controller contiene i metodi per eseguire le operazioni CRUD
Operazioni implemantate:
-test tramite get
-richiesta di un singolo ogetto tramite GET
-inserimento di un nuovo oggetto tramite POST
-update di un oggetto tramite PUT
-elimazione di un prodotto tramite DELETE

### Inviare comandi all' API tramite _curl_
* Test dell'api
* _curl https://simple-app-devops.herokuapp.com/products/test_ 
* Creazione di un prodotto nel db
* _curl -d "name=NAMEPRODUCT&price=COSTPRODUCT" -X POST https://simple-app-devops.herokuapp.com/products/create_
* Lettura di un prodotto nel db per id
* _curl https://simple-app-devops.herokuapp.com/products/NAMEPRODUCT_
* Update di un prodotto
* _curl -d "DATATOUPDATE" -X PUT https://simple-app-devops.herokuapp.com/products/NAMEPRODUCT/update_
* Delete di un prodotto_
* _curl -X DELETE https://simple-app-devops.herokuapp.com/products/NAMEPRODUCT/delete_


## Pipeline CI/CD
* dev:  
  _modifica del codice sorgente_
* commit:  
    _alla commit sul branch principale  della repo GitLab viene eseguita in_
    _modo automatico la richiesta di build e di distribuzione_
* build:  
    _mediante npm_
* test:  
    _se i test sulle operazioni CRUD passano si passa allo stage di prod_
    _se i test sulle operazioni CRUD non passano la pipeline viene intorrota ed_
        _il programmatore/team viene notificato con un email_
* prod:  
    _app avviata in heroku, pronta a ricevere richieste HTTP_



