const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const product_controller = require('../controllers/product.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', product_controller.test);

router.post('/create', product_controller.product_create);

router.get('/:name', product_controller.product_details_name);

router.put('/:name/update', product_controller.product_update_name);

router.delete('/:name/delete', product_controller.product_delete_name);

module.exports = router;


